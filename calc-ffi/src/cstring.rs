// SPDX-FileCopyrightText: 2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

use std::result::Result;

#[derive(Debug, PartialEq, Eq)]
pub enum CStringError {
    NullPtr,
    InvalidString,
}

pub unsafe fn clone_as_string(c_string: *const libc::c_char) -> Result<String, CStringError> {
    if c_string.is_null() {
        return Err(CStringError::NullPtr);
    }
    Ok(std::ffi::CStr::from_ptr(c_string)
        .to_str()
        .map_err(|_| CStringError::InvalidString)?
        .to_owned())
}
