use calc::prelude::*;
use cstring::clone_as_string;

use std::os::raw::c_char;
use std::ptr::null_mut;
use std::result;
mod cstring;
use crate::cstring::CStringError;
pub use calc::Expr;

#[repr(C)]
pub enum ReturnCode {
    Ok = 0,
    NullPtr,
    InvalidString,
    UnexpectedInput,
    InsufficientNumbers,
    EmptyStack,
    ExcessStack,
    DivideByZero,
}

impl From<CStringError> for ReturnCode {
    fn from(e: CStringError) -> Self {
        match e {
            CStringError::NullPtr => ReturnCode::NullPtr,
            CStringError::InvalidString => ReturnCode::InvalidString,
        }
    }
}

impl From<ParseError> for ReturnCode {
    fn from(e: ParseError) -> Self {
        match e {
            ParseError::UnexpectedInput(_) => ReturnCode::UnexpectedInput,
            ParseError::InsufficientNumbers => ReturnCode::InsufficientNumbers,
            ParseError::EmptyStack => ReturnCode::EmptyStack,
            ParseError::ExcessStack => ReturnCode::ExcessStack,
        }
    }
}
impl From<EvalError> for ReturnCode {
    fn from(e: EvalError) -> Self {
        match e {
            EvalError::DivideByZero => ReturnCode::DivideByZero,
        }
    }
}

impl From<result::Result<(), ReturnCode>> for ReturnCode {
    fn from(res: result::Result<(), ReturnCode>) -> Self {
        match res {
            Ok(_) => ReturnCode::Ok,
            Err(e) => e.into(),
        }
    }
}

fn parse_from_cstr(maybe_cstr: *const c_char) -> result::Result<Expr, ReturnCode> {
    let s = unsafe { clone_as_string(maybe_cstr)? };

    let expr = calc::parse::parse(&s)?;
    Ok(expr)
}

fn parse_and_eval_impl(
    maybe_cstr: *const c_char,
    output: *mut i64,
) -> result::Result<(), ReturnCode> {
    let output = unsafe { output.as_mut() }.ok_or(ReturnCode::NullPtr)?;
    let expr = parse_from_cstr(maybe_cstr)?;
    let answer = eval(&expr)?;
    *output = answer;
    Ok(())
}

#[no_mangle]
pub unsafe extern "C" fn parse_and_eval(maybe_cstr: *const c_char, output: *mut i64) -> ReturnCode {
    parse_and_eval_impl(maybe_cstr, output).into()
}

/// This will return null if unsuccessful
#[no_mangle]
pub unsafe extern "C" fn c_parse(maybe_cstr: *const c_char) -> *mut Expr {
    match parse_from_cstr(maybe_cstr) {
        Ok(expr) => {
            // return a raw pointer to an object whose ownership
            // has been given up on purpose (a "leak")
            // so that it can be freely used in the FFI caller
            // who later has to call our release function
            Box::leak(Box::new(expr))
        }
        Err(_) => null_mut(),
    }
}

fn c_eval_impl(expr: *const Expr, output: *mut i64) -> result::Result<(), ReturnCode> {
    let output = unsafe { output.as_mut() }.ok_or(ReturnCode::NullPtr)?;
    let expr = unsafe { expr.as_ref() }.ok_or(ReturnCode::NullPtr)?;
    let answer = eval(expr)?;
    *output = answer;
    Ok(())
}

#[no_mangle]
pub unsafe extern "C" fn c_eval(expr: *const Expr, output: *mut i64) -> ReturnCode {
    c_eval_impl(expr, output).into()
}

#[no_mangle]
/// release objects whose ownership was previously passed to C
pub unsafe extern "C" fn release_expr(box_expr: *mut Expr) {
    // take ownership back and go out of scope to drop
    if !box_expr.is_null() {
        let _ = Box::from_raw(box_expr);
    }
}
