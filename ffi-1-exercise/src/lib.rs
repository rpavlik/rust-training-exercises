use std::ffi::{CStr, CString, NulError};

use libc::c_void;

extern crate leveldb_sys;

mod wrapped {
    use std::{
        ffi::CStr,
        ptr::{self, NonNull},
    };

    use libc::c_char;

    pub(crate) struct DbOptions(NonNull<leveldb_sys::leveldb_options_t>);
    impl DbOptions {
        pub(crate) fn new() -> Option<Self> {
            unsafe { NonNull::new(leveldb_sys::leveldb_options_create()).map(|p| DbOptions(p)) }
        }

        pub(crate) const fn as_ptr(&self) -> *mut leveldb_sys::leveldb_options_t {
            self.0.as_ptr()
        }

        pub(crate) fn set_create_if_missing(&mut self, val: bool) {
            unsafe { leveldb_sys::leveldb_options_set_create_if_missing(self.as_ptr(), val as u8) }
        }
    }
    impl Drop for DbOptions {
        fn drop(&mut self) {
            unsafe { leveldb_sys::leveldb_options_destroy(self.as_ptr()) }
        }
    }

    #[derive(Debug)]
    pub(crate) struct LevelDB(NonNull<leveldb_sys::leveldb_t>);

    impl LevelDB {
        pub(crate) unsafe fn open(name: &CStr, options: &DbOptions) -> (Option<Self>, *mut c_char) {
            let mut err = ptr::null_mut::<i8>();
            let db = NonNull::new(leveldb_sys::leveldb_open(
                options.as_ptr(),
                name.as_ptr(),
                &mut err,
            ))
            .map(|p| LevelDB(p));
            (db, err)
        }

        pub(crate) const fn as_ptr(&self) -> *mut leveldb_sys::leveldb_t {
            self.0.as_ptr()
        }
    }
    impl Drop for LevelDB {
        fn drop(&mut self) {
            unsafe { leveldb_sys::leveldb_close(self.as_ptr()) }
        }
    }
}

#[derive(Debug)]
pub enum Error {
    NameError(NulError),
    LevelDBError(String),
    UnspecifiedCreationFailureError,
}
impl From<NulError> for Error {
    fn from(e: NulError) -> Self {
        Self::NameError(e)
    }
}

#[derive(Debug)]
pub struct Database(wrapped::LevelDB);

pub type Result<T> = std::result::Result<T, Error>;

impl Database {
    pub fn open(name: &str, create_if_missing: bool) -> Result<Self> {
        let mut options =
            wrapped::DbOptions::new().ok_or(Error::UnspecifiedCreationFailureError)?;
        options.set_create_if_missing(create_if_missing);
        Database::new(name, &options)
    }

    fn new(name: &str, options: &wrapped::DbOptions) -> Result<Self> {
        let name = CString::new(name)?;

        unsafe {
            let (db_impl, err) = wrapped::LevelDB::open(&name, options);

            if !err.is_null() {
                let rusty_err = CStr::from_ptr(err).to_string_lossy().to_string();
                libc::free(err as *mut c_void);
                return Err(Error::LevelDBError(rusty_err));
            }

            let db_impl = db_impl.ok_or(Error::UnspecifiedCreationFailureError)?;
            Ok(Database(db_impl))
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate tempdir;
    use tempdir::TempDir;

    use crate::{wrapped::DbOptions, Database};

    fn make_tempdir() -> TempDir {
        TempDir::new("leveldb_test").unwrap()
    }

    #[test]
    fn options() {
        let options = DbOptions::new().unwrap();
        assert!(!options.as_ptr().is_null());
    }

    #[test]
    fn open_existing_dir() {
        let tempdir = make_tempdir();
        let db_dir = tempdir.path();
        let db = Database::open(&db_dir.to_str().unwrap(), true);
        assert!(db.is_ok());
    }

    #[test]
    fn open_existing_dir_no_create() {
        let tempdir = make_tempdir();
        let db_dir = tempdir.path();
        let maybe_db = Database::open(&db_dir.to_str().unwrap(), false);
        assert!(maybe_db.is_err());
        eprintln!("our error (which we expected): {:?}", maybe_db.unwrap_err());
    }

    #[test]
    fn open_subdir_not_existing_create_if_missing() {
        let tempdir = make_tempdir();
        let db_dir = tempdir.path().join("testdb");
        let db = Database::open(&db_dir.to_str().unwrap(), true);
        assert!(db.is_ok());
    }

    #[test]
    fn open_subdir_not_existing_error() {
        let tempdir = make_tempdir();
        let db_dir = tempdir.path().join("testdb");
        let maybe_db = Database::open(&db_dir.to_str().unwrap(), false);
        assert!(maybe_db.is_err());
        eprintln!("our error (which we expected): {:?}", maybe_db.unwrap_err());
    }

    #[test]
    fn open_inaccessible() {
        let bad_dir = if cfg!(windows) {
            "/windows/system32"
        } else {
            "/etc/shadow"
        };
        let maybe_db = Database::open(bad_dir, true);

        assert!(maybe_db.is_err());
        eprintln!("our error (which we expected): {:?}", maybe_db.unwrap_err());
    }
}
