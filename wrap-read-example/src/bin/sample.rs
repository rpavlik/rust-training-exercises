// Copyright 2018-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
// Author: Ryan A. Pavlik <ryan.pavlik@collabora.com>

use std::io::{self, ErrorKind};

use async_std::{io::stdin, net::TcpListener, prelude::*, task};
use futures::AsyncRead;
use wrap_read_example::{error::VrpnError, message_stream::AsyncReadMessagesExt};

const MY_MESSAGE: &[u8] = b"hellohellohello";

async fn process<T: AsyncRead + Unpin>(stream: T) -> std::result::Result<(), VrpnError> {
    // wrap in our messages thingie
    let mut stream = AsyncReadMessagesExt::messages(stream);
    loop {
        match stream.next().await {
            Some(Ok(msg)) => {
                eprintln!("Got message: {:?}", msg);
            }
            Some(Err(e)) => {
                eprintln!("Got error {:?}", e);
                return Err(e);
            }
            None => {
                eprintln!("EOF reached (?)");
                return Ok(());
            }
        }
    }
}

async fn tcp_listen() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:5555").await?;
    println!(
        "Connect to {} and send 'hello' repeatedly with no other characters",
        listener.local_addr()?
    );
    let mut incoming = listener.incoming();

    while let Some(stream) = incoming.next().await {
        let stream = stream?;
        task::spawn(async {
            process(stream).await.unwrap();
        });
    }
    Ok(())
}

async fn use_stdin() -> std::result::Result<(), VrpnError> {
    println!("type 'hello' repeatedly with no other characters");
    process(stdin()).await
}

const POLLS_BETWEEN_READY: usize = 3;

struct CannedMessage {
    position: usize,
    poll_counter: usize,
}
impl CannedMessage {
    fn new() -> CannedMessage {
        CannedMessage {
            position: 0,
            poll_counter: 0,
        }
    }
}
// impl Stream for CannedMessage {
//     type Item = u8;

//     fn poll_next(self: std::pin::Pin<&mut Self>, cx: &mut task::Context<'_>) -> task::Poll<Option<Self::Item>> {
//         todo!()
//     }
// }

impl AsyncRead for CannedMessage {
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut task::Context<'_>,
        buf: &mut [u8],
    ) -> task::Poll<io::Result<usize>> {
        self.as_mut().poll_counter = (self.poll_counter + 1) % POLLS_BETWEEN_READY;
        // queue another poll, we are not really waiting on anything for real
        cx.waker().wake_by_ref();
        if self.poll_counter == 0 {
            if self.position < MY_MESSAGE.len() {
                eprintln!(
                    "yielding character {} of {}",
                    self.position,
                    MY_MESSAGE.len()
                );
                buf[0] = MY_MESSAGE[self.position];
                self.position += 1;
                task::Poll::Ready(Ok(1))
            } else {
                // task::Poll::Ready(Ok(0))
                task::Poll::Ready(Err(io::Error::new(
                    ErrorKind::UnexpectedEof,
                    "no more bytes",
                )))
            }
        } else {
            task::Poll::Pending
        }
    }
}
async fn use_constant() -> std::result::Result<(), VrpnError> {
    process(CannedMessage::new()).await
}
fn main() -> std::result::Result<(), VrpnError> {
    // task::block_on(tcp_listen())
    // task::block_on(use_stdin())
    task::block_on(use_constant())
}
