// Copyright 2018-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
// Author: Ryan A. Pavlik <ryan.pavlik@collabora.com>

use bytes::Buf;

use crate::{
    error::{BufferUnbufferError, UnbufferResult},
    size_requirement::SizeRequirement,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SequencedGenericMessage {}

const EXPECTED: &[u8] = b"hello";

impl SequencedGenericMessage {
    /// Deserialize from a buffer.
    ///
    /// In case of error, your buffer is unmodified.
    pub fn try_read_from_buf<T: Buf + Clone>(buf: &mut T) -> UnbufferResult<Self> {
        let message_size = EXPECTED.len();
        let initial_remaining = buf.remaining();
        if initial_remaining < message_size {
            return Err(BufferUnbufferError::from(SizeRequirement::AtLeast(
                message_size - initial_remaining,
            )));
        }

        let mut local_buf = buf.clone();
        let my_bytes = local_buf.copy_to_bytes(message_size);
        if my_bytes == EXPECTED {
            buf.advance(message_size);
            Ok(SequencedGenericMessage {})
        } else {
            Err(BufferUnbufferError::ParseError {
                parsing_kind: "generic".to_string(),
                s: "mismatch".to_string(),
            })
        }
    }
}
