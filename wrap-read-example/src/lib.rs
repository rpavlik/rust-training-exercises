// Copyright 2018-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
// Author: Ryan A. Pavlik <ryan.pavlik@collabora.com>

extern crate bytes;
extern crate pin_project_lite;

pub mod error;
pub mod message_stream;
pub mod mock;
pub mod size_requirement;
