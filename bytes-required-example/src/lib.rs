// Copyright 2018-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
// Author: Ryan A. Pavlik <ryan.pavlik@collabora.com>

use std::{
    convert::TryFrom,
    fmt::{self, Display},
    net::AddrParseError,
    num::ParseIntError,
    ops::Add,
};
use thiserror::Error;

/// Expresses how many more bytes we require/expect when parsing a message.
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum BytesRequired {
    Exactly(usize),
    AtLeast(usize),
    Unknown,
}

impl BytesRequired {
    /// Compares a byte requirement to available bytes in a buffer.
    ///
    /// Note that in this case, Exactly(c) is satisfied by c or anything larger.
    pub fn satisfied_by(&self, buf_size: usize) -> Option<bool> {
        match *self {
            BytesRequired::Exactly(c) => Some(c <= buf_size),
            BytesRequired::AtLeast(c) => Some(c <= buf_size),
            BytesRequired::Unknown => None,
        }
    }

    /// Maps `BytesRequired::Exactly(n)` to `BytesRequired::AtLeast(n)`
    pub fn expand(self) -> BytesRequired {
        match self {
            BytesRequired::Exactly(n) => BytesRequired::AtLeast(n),
            BytesRequired::AtLeast(n) => BytesRequired::AtLeast(n),
            BytesRequired::Unknown => BytesRequired::Unknown,
        }
    }
}

impl Add for BytesRequired {
    type Output = BytesRequired;
    fn add(self, other: BytesRequired) -> Self::Output {
        use self::BytesRequired::*;

        match (self, other) {
            (Exactly(a), Exactly(b)) => Exactly(a + b),
            (AtLeast(a), Exactly(b)) => AtLeast(a + b),
            (Exactly(a), AtLeast(b)) => AtLeast(a + b),
            (AtLeast(a), AtLeast(b)) => AtLeast(a + b),
            // Anything else has Unknown as one term.
            _ => Unknown,
        }
    }
}

impl Display for BytesRequired {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            BytesRequired::Exactly(n) => write!(f, "exactly {}", n),
            BytesRequired::AtLeast(n) => write!(f, "at least {}", n),
            BytesRequired::Unknown => write!(f, "unknown"),
        }
    }
}

/// A minimal "error" indicating that an error did not contain a BytesRequired value.
pub struct DoesNotContainBytesRequired(());

/// Error type returned by buffering/unbuffering.
#[derive(Error, Debug)]
pub enum BufferUnbufferError {
    #[error("unbuffering ran out of buffered bytes: need {0} additional bytes")]
    NeedMoreData(BytesRequired),
    // #[error("unexpected data: expected '{expected:?}', got '{actual:?}'")]
    // UnexpectedAsciiData { actual: Bytes, expected: Bytes },
    #[error("buffering ran out of buffer space")]
    OutOfBuffer,
    #[error("according to a length field we have complete data, but we need at least {0} additional bytes")]
    HeaderSizeMismatch(String),
    #[error("Error parsing {parsing_kind}: {s}")]
    ParseError { parsing_kind: String, s: String },
}

impl From<BytesRequired> for BufferUnbufferError {
    fn from(val: BytesRequired) -> Self {
        BufferUnbufferError::NeedMoreData(val)
    }
}

impl From<ParseIntError> for BufferUnbufferError {
    fn from(e: ParseIntError) -> Self {
        BufferUnbufferError::ParseError {
            parsing_kind: "integer".to_string(),
            s: e.to_string(),
        }
    }
}

impl From<AddrParseError> for BufferUnbufferError {
    fn from(e: AddrParseError) -> Self {
        BufferUnbufferError::ParseError {
            parsing_kind: "IP address".to_string(),
            s: e.to_string(),
        }
    }
}

impl TryFrom<BufferUnbufferError> for BytesRequired {
    type Error = DoesNotContainBytesRequired;

    fn try_from(value: BufferUnbufferError) -> std::result::Result<Self, Self::Error> {
        if let BufferUnbufferError::NeedMoreData(required) = value {
            Ok(required)
        } else {
            Err(DoesNotContainBytesRequired(()))
        }
    }
}

impl TryFrom<&BufferUnbufferError> for BytesRequired {
    type Error = DoesNotContainBytesRequired;

    fn try_from(value: &BufferUnbufferError) -> std::result::Result<Self, Self::Error> {
        if let BufferUnbufferError::NeedMoreData(required) = value {
            Ok(*required)
        } else {
            Err(DoesNotContainBytesRequired(()))
        }
    }
}

impl BufferUnbufferError {
    /// Maps `BufferUnbufferError::NeedMoreData(BytesRequired::Exactly(n))` to
    /// `BufferUnbufferError::NeedMoreData(BytesRequired::AtLeast(n))`
    pub fn expand_bytes_required(self) -> BufferUnbufferError {
        if let BufferUnbufferError::NeedMoreData(required) = self {
            return BufferUnbufferError::NeedMoreData(required.expand());
        }
        self
    }

    /// Maps `BufferUnbufferError::NeedMoreData(_)` to `BufferUnbufferError::HeaderSizeMismatch(_)`
    pub fn map_bytes_required_to_size_mismatch(self) -> BufferUnbufferError {
        if let BufferUnbufferError::NeedMoreData(required) = self {
            return BufferUnbufferError::HeaderSizeMismatch(required.to_string());
        }
        self
    }
}

/// Error type for the main VRPN crate
#[derive(Error, Debug)]
pub enum Error {
    #[error("{0}")]
    BufferUnbuffer(#[from] BufferUnbufferError),
    // #[error("invalid id {0}")]
    // InvalidId(IdType),
    #[error("empty translation table entry")]
    EmptyEntry,
    #[error("too many handlers")]
    TooManyHandlers,
    #[error("too many mappings")]
    TooManyMappings,
    #[error("handler not found")]
    HandlerNotFound,
    #[error("handler returned an error")]
    GenericErrorReturn,
    #[error("a non-system message was forwarded to Endpoint::handle_message_as_system()")]
    NotSystemMessage,
    // #[error("un-recognized system message id {0}")]
    // UnrecognizedSystemMessage(IdType),
    // #[error("version mismatch: expected something compatible with {expected}, got {actual}")]
    // VersionMismatch { actual: Version, expected: Version },
    #[error("{0}")]
    Other(#[from] Box<dyn std::error::Error + Send>),
    #[error("{0}")]
    OtherMessage(String),
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Other(Box::new(e))
    }
}

impl TryFrom<Error> for BytesRequired {
    type Error = DoesNotContainBytesRequired;

    fn try_from(value: Error) -> std::result::Result<Self, Self::Error> {
        if let Error::BufferUnbuffer(buf_unbuf) = value {
            BytesRequired::try_from(buf_unbuf)
        } else {
            Err(DoesNotContainBytesRequired(()))
        }
    }
}

impl TryFrom<&Error> for BytesRequired {
    type Error = DoesNotContainBytesRequired;

    fn try_from(value: &Error) -> std::result::Result<Self, Self::Error> {
        if let Error::BufferUnbuffer(buf_unbuf) = value {
            BytesRequired::try_from(buf_unbuf)
        } else {
            Err(DoesNotContainBytesRequired(()))
        }
    }
}

impl From<BytesRequired> for Error {
    fn from(val: BytesRequired) -> Self {
        Error::BufferUnbuffer(BufferUnbufferError::from(val))
    }
}

impl Error {
    /// Maps `Error::BufferUnbuffer(BufferUnbufferError::NeedMoreData(BytesRequired::Exactly(n)))` to
    /// `Error::BufferUnbuffer((BufferUnbufferError::NeedMoreData(BytesRequired::AtLeast(n)))`
    pub fn expand_bytes_required(self) -> Error {
        if let Error::BufferUnbuffer(err) = self {
            return Error::BufferUnbuffer(err.expand_bytes_required());
        }
        self
    }
    /// Maps `BufferUnbufferError::NeedMoreData(_)` to `BufferUnbufferError::HeaderSizeMismatch(_)`
    pub fn map_bytes_required_to_size_mismatch(self) -> Error {
        if let Error::BufferUnbuffer(err) = self {
            return Error::BufferUnbuffer(err.map_bytes_required_to_size_mismatch());
        }
        self
    }
}

impl Error {
    pub fn is_need_more_data(&self) -> bool {
        BytesRequired::try_from(self).is_ok()
    }

}

impl<T> From<std::sync::PoisonError<T>> for Error {
    fn from(v: std::sync::PoisonError<T>) -> Error {
        Error::OtherMessage(v.to_string())
    }
}

pub type Result<T> = std::result::Result<T, Error>;

pub type EmptyResult = Result<()>;


pub fn expand_bytes_required<T>(val: T) -> T
where
    T: Copy + From<BytesRequired>,
    BytesRequired: TryFrom<T>,
{
    match BytesRequired::try_from(val) {
        Ok(required) => T::from(required.expand()),
        Err(_) => val,
    }
}
