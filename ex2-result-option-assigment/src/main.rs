use std::fs::File;
use std::io::{prelude::*, BufReader, Result};
use url::Url;

fn parse_url(line: String) -> Option<Url> {
    match Url::parse(&line) {
        Ok(url) => Some(url),
        Err(_) => None,
    }
}

fn main() {
    let f = File::open("src/data/content.txt");

    match f {
        Ok(file) => {
            println!("Opened");
            let reader = BufReader::new(file);
            let decoded_lines = reader
                .lines()
                .collect::<Result<Vec<_>>>()
                .expect("Cannot decode a line");

            println!("lines read: {}", decoded_lines.len());
            let urls = decoded_lines
                .into_iter()
                .filter(|l| !l.is_empty())
                .filter_map(|l| parse_url(l));
            for url in urls {
                println!("{}", url);
            }
        }
        Err(e) => panic!("Error: {}", e),
    };
}
