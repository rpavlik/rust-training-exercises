use std::env;
use std::path::PathBuf;
use std::process::Command;

fn main() {
    println!("cargo:rerun-if-changed=build.rs");
    let crate_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    Command::new("bash").args(["-c", "env"]).status().unwrap();
    let package_name = env::var("CARGO_PKG_NAME").unwrap();
    let out_dir = env::var("OUT_DIR").unwrap();
    let out_dir = PathBuf::from(out_dir);
    let target_dir = out_dir
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .parent()
        .unwrap();
    let output_file = target_dir
        .join(format!("{}.hpp", package_name))
        .display()
        .to_string();
    cbindgen::Builder::new()
        .with_crate(crate_dir)
        .with_language(cbindgen::Language::Cxx)
        .generate()
        .expect("Binding failure")
        .write_to_file(output_file);
}
