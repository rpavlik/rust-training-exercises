use std::result::Result;

#[repr(C)]
pub enum ReturnCode {
    Success = 0,
    NullPtr = -1,
    Invalid = -2,
}

#[derive(Debug, PartialEq, Eq)]
enum CStringError {
    NullPtr,
    Invalid,
}

impl From<CStringError> for ReturnCode {
    fn from(e: CStringError) -> Self {
        match e {
            CStringError::NullPtr => ReturnCode::NullPtr,
            CStringError::Invalid => ReturnCode::Invalid,
        }
    }
}

impl<E: Into<ReturnCode>> From<Result<(), E>> for ReturnCode {
    fn from(result: Result<(), E>) -> Self {
        match result {
            Ok(()) => ReturnCode::Success,
            Err(e) => e.into(),
        }
    }
}
unsafe fn hello_impl(c_string: *const libc::c_char) -> Result<(), CStringError> {
    let s = clone_as_string(c_string)?;
    println!("{}", s);
    Ok(())
}

#[no_mangle]
pub unsafe extern "C" fn hello(c_string: *const libc::c_char) -> ReturnCode {
    hello_impl(c_string).into()
}

#[cfg(test)]
mod tests {
    use std::{ffi::CString, ptr::null};

    use crate::{clone_as_string, CStringError};

    #[test]
    fn success() {
        let orig_str = "test";
        let cstring = CString::new(&orig_str[..]).expect("should be able to create cstring");
        let cloned =
            unsafe { clone_as_string(cstring.as_ptr()) }.expect("should succeed in cloning");
        assert_eq!(orig_str, cloned);
    }
    #[test]
    fn nullptr() {
        let result = unsafe { clone_as_string(null()) };
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), CStringError::NullPtr);
    }
}
