// Copyright 2018-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
// Author: Ryan A. Pavlik <ryan.pavlik@collabora.com>

extern crate bytes;

use std::convert::TryFrom;

use bytes::{Buf, BufMut, Bytes, BytesMut};

/*----------------- MessageHeader -----------------*/
/// Header information for a message.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct MessageHeader {
    /// Mock header contents - replacing more complex stuff for sake of example
    pub header_data: i32,
}

impl MessageHeader {
    /// Constructor for a message header
    pub fn new(header_data: i32) -> MessageHeader {
        MessageHeader { header_data }
    }
}

/*----------------- Message<T> -----------------*/
/// A message with header information, almost ready to be buffered to the wire.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Message<T: MessageBody> {
    pub header: MessageHeader,
    pub body: T,
}

impl<T: MessageBody> Message<T> {
    pub fn new(header_data: i32, body: T) -> Message<T> {
        Message {
            header: MessageHeader::new(header_data),
            body,
        }
    }

    /// Create a message by combining a header and a body.
    pub fn from_header_and_body(header: MessageHeader, body: T) -> Message<T> {
        Message { header, body }
    }
}
/*----------------- GenericBody -----------------*/
/// Generic body struct used in unbuffering process, before dispatch on type to fully decode.
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct GenericBody {
    pub inner: Bytes,
}

impl GenericBody {
    pub fn new(inner: Bytes) -> GenericBody {
        GenericBody { inner }
    }
}

impl Default for GenericBody {
    fn default() -> GenericBody {
        GenericBody::new(Bytes::default())
    }
}
impl MessageBody for GenericBody {}

/// A special type of message, with just an (exact-size) buffer as the body.
pub type GenericMessage = Message<GenericBody>;

/*----------------- Misc Traits -----------------*/
/// Empty trait used to indicate types that can be placed in a message body.
pub trait MessageBody {}

/// Trait for typed message bodies.
pub trait TypedMessageBody: std::fmt::Debug {
    /// The name string (for user messages) or type ID (for system messages) used to identify this message type.
    const MESSAGE_IDENTIFIER: i32;
}

impl<T> MessageBody for T where T: TypedMessageBody {}

/*----------------- Unbuffer -----------------*/
/// Trait for types that can be "unbuffered" (parsed from a byte buffer)
pub trait Unbuffer: Sized {
    /// Tries to unbuffer, advancing the buffer position only if successful.
    ///
    /// Returns `Err(BufferUnbufferError::NeedMoreData(n))` if not enough data.
    fn unbuffer_ref<T: Buf>(buf: &mut T) -> UnbufferResult<Self>;
}

pub type UnbufferResult<T> = std::result::Result<T, String>;

/*----------------- Buffer -----------------*/

/// Trait for computing the buffer size needed for types
/// that can be "buffered" (serialized to a byte buffer),
pub trait BufferSize {
    /// Indicates the number of bytes required in the buffer to store this.
    fn buffer_size(&self) -> usize;
}

/// Trait for types that can be "buffered" (serialized to a byte buffer)
pub trait Buffer: BufferSize {
    /// Serialize to a buffer (taken as a mutable reference)
    fn buffer_ref<T: BufMut>(&self, buf: &mut T) -> std::result::Result<(), String>;

    /// Get the number of bytes required to serialize this to a buffer.
    fn required_buffer_size(&self) -> usize {
        self.buffer_size()
    }
}

/*----------------- parse generic into typed -----------------*/

impl<T: TypedMessageBody + Unbuffer> TryFrom<&GenericMessage> for Message<T> {
    type Error = String;

    /// Try parsing a generic message into a typed message
    ///
    /// # Errors
    /// - If the unbuffering of the given type fails
    /// - If the generic message's body isn't fully consumed by the typed message body
    fn try_from(msg: &GenericMessage) -> std::result::Result<Self, Self::Error> {
        let mut buf = msg.body.inner.clone();
        let body = T::unbuffer_ref(&mut buf).map_err(|e| e.to_string())?;
        if !buf.is_empty() {
            return Err(format!(
                "message body length was indicated as {}, but {} bytes remain unconsumed",
                msg.body.inner.len(),
                buf.len()
            ));
        }
        Ok(Message::from_header_and_body(msg.header.clone(), body))
    }
}

/*----------------- serialize typed into generic -----------------*/

// FIXME: Why doesn't this work?

impl<T> TryFrom<T> for GenericBody
where
    T: TypedMessageBody + Buffer,
{
    type Error = String;

    /// Try converting a typed message into a generic message
    ///
    /// # Errors
    /// If buffering fails.
    fn try_from(value: T) -> std::result::Result<Self, Self::Error> {
        let old_body = value.body;
        let header = value.header;
        let generic = BytesMut::new().allocate_and_buffer(old_body).map(|body| {
            GenericMessage::from_header_and_body(header, GenericBody::new(body.freeze()))
        })?;
        Ok(generic)
    }
}

fn main() {
    println!("Hello, world!");
}
