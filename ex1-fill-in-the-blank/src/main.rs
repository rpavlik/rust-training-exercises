#[derive(PartialEq, Debug)]
enum FarmAnimal {
    Worm,
    Cow,
    Bull,
    Chicken { num_eggs: usize },
    Dog { name: String },
}

fn what_does_the_animal_say(animal: &FarmAnimal) {
    let noise = match animal {
        FarmAnimal::Cow => "moo".to_string(),
        FarmAnimal::Bull => "moo".to_string(),
        FarmAnimal::Chicken { num_eggs: _ } => "cluck, cluck!".to_string(),
        FarmAnimal::Dog { name } => {
            if name == "Lassie" {
                "Timmy fell down a well".to_string()
            } else {
                format!("woof, woof! I am {}!", name)
            }
        }
        FarmAnimal::Worm => "-- (silence)".to_string(),
    };

    println!("{:?} says: {:?}", animal, noise);
}

fn main() {
    what_does_the_animal_say(&FarmAnimal::Dog {
        name: "Lassie".to_string(),
    });
    what_does_the_animal_say(&FarmAnimal::Cow);
    what_does_the_animal_say(&FarmAnimal::Bull);
    what_does_the_animal_say(&FarmAnimal::Chicken { num_eggs: 3 });
    what_does_the_animal_say(&FarmAnimal::Worm);
}
